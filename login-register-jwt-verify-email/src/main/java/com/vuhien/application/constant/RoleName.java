package com.vuhien.application.constant;

/**
 * Created by VuHien96 on 17/07/2021 14:27
 */
public enum RoleName {
    USER,
    ADMIN,
    PM
}
