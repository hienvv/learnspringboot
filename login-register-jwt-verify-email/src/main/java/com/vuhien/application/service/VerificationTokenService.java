package com.vuhien.application.service;

import com.vuhien.application.entity.User;
import com.vuhien.application.entity.VerificationToken;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by VuHien96 on 17/07/2021 15:27
 */
@Service
public interface VerificationTokenService {
    String createVerificationToken(User user);
    Optional<VerificationToken> getVerificationToken(String token);
    void updateConfirmedAt(String token);
}
