package com.vuhien.application.service;

import com.vuhien.application.entity.User;
import com.vuhien.application.model.request.RegisterRequest;
import org.springframework.stereotype.Service;

/**
 * Created by VuHien96 on 17/07/2021 14:53
 */
@Service
public interface UserService {
    User createUser(RegisterRequest request);
    User updateUser(RegisterRequest request, Long id);
    void deleteUser(Long id);
    void confirmToken(String token);
}
