package com.vuhien.application.service.impl;

import com.vuhien.application.entity.User;
import com.vuhien.application.entity.VerificationToken;
import com.vuhien.application.exception.BadRequestException;
import com.vuhien.application.repository.VerificationTokenRepository;
import com.vuhien.application.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by VuHien96 on 17/07/2021 15:28
 */
@Component
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Override
    public String createVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setCreatedAt(LocalDateTime.now());
        //Xét token có hạn trong 15 phút
        verificationToken.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        verificationToken.setUser(user);
        verificationTokenRepository.save(verificationToken);
        return token;
    }

    @Override
    public Optional<VerificationToken> getVerificationToken(String token) {
        Optional<VerificationToken> verificationToken = verificationTokenRepository.findByToken(token);
        if (verificationToken.isEmpty()){
            throw new BadRequestException("Không tìm thấy token");
        }
        return verificationToken;
    }

    @Override
    public void updateConfirmedAt(String token) {
        verificationTokenRepository.updateConfirmedAt(token, LocalDateTime.now());
    }
}
