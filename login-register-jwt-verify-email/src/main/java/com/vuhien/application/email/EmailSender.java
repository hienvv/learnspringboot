package com.vuhien.application.email;

public interface EmailSender {
    void send(String to, String email);
}
