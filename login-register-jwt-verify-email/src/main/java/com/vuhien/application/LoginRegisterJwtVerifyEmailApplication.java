package com.vuhien.application;

import com.vuhien.application.Config.Swagger2Config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@Import(Swagger2Config.class)
public class LoginRegisterJwtVerifyEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginRegisterJwtVerifyEmailApplication.class, args);
    }

}
