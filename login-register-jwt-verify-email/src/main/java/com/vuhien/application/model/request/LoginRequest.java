package com.vuhien.application.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Created by VuHien96 on 17/07/2021 14:52
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class LoginRequest {
    @Email(message = "Email không đúng")
    @NotBlank(message = "Email này trống")
    private String email;
    @NotBlank
    @Size(min = 6,max = 20,message = "Độ dài mật khẩu trong khoảng 6-20 ký tự")
    private String password;
}
