package com.vuhien.application.model.mapper;

import com.vuhien.application.entity.User;
import com.vuhien.application.model.dto.UserDTO;
import com.vuhien.application.model.request.RegisterRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.time.LocalDateTime;

/**
 * Created by VuHien96 on 17/07/2021 15:11
 */

public class UserMapper {
    public static UserDTO toUserDTO(User user) {
        return null;
    }

    public static User toUser(RegisterRequest request) {
        User user = new User();
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());
        String hash = BCrypt.hashpw(request.getPassword(), BCrypt.gensalt(12));
        user.setPassword(hash);
        user.setPhone(request.getPhone());
        user.setStatus(false);
        user.setCreatedAt(LocalDateTime.now());
        return user;
    }
}
