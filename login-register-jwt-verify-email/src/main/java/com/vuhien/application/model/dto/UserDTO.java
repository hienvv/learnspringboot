package com.vuhien.application.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by VuHien96 on 17/07/2021 14:52
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserDTO {
    private Long id;
    private String fullName;
    private String email;
    private String password;
    private String phone;
    private String address;
    private String avatar;
    private Boolean status;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
}
