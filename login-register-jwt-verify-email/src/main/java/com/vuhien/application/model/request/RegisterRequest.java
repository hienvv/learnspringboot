package com.vuhien.application.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by VuHien96 on 17/07/2021 14:52
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class RegisterRequest {
    @NotBlank(message = "Tên trống")
    private String fullName;
    @Email(message = "Email không đúng")
    @NotBlank(message = "Email trống")
    private String email;
    @NotBlank
    @Size(min = 6, max = 20, message = "Độ dài mật khẩu trong khoảng 6-20 ký tự")
    private String password;
    @Pattern(regexp = "(84|0[3|5|7|8|9])+([0-9]{8})\\b", message = "Số điện thoại không đúng")
    private String phone;
    private Set<String> roles;
}
